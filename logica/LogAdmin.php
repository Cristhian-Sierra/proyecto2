<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogAdminDAO.php";



class LogAdmin{
    
    private $id;
    private $id_admin;
    private $fecha;
    private $hora;
    private $accion;
    private $nombre;
    private $apellido;
    private $correo;
    private $conexion;
    private $logadminDAO;

    public function getId(){
        return $this -> id;
    }
    
    public function getId_admin(){
        return $this -> id_admin;
    }

    public function getFecha(){
        return $this -> fecha;
    }

    public function getHora(){
        return $this -> hora;
    }
    

    public function getAccion(){
        return $this -> accion;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    public function getApellido(){
        return $this -> apellido;
    }
    public function getCorreo(){
        return $this -> correo;
    }
    
    public function LogAdmin($id = "", $fecha = "", $hora = "", $accion = "", $id_admin="",$nombre="",$apellido="",$correo=""){
        $this -> id = $id;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> accion = $accion;
        $this -> id_admin = $id_admin;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        
       
        $this -> conexion = new Conexion();
        $this -> logadminDAO = new LogAdminDAO($this -> id, $this -> fecha, $this -> hora, $this ->accion,$this -> id_admin,$this->nombre,$this->apellido,$this->correo);
    }


    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logadminDAO -> consultar());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new LogAdmin($registro[0], $registro[1], $registro[2], $registro[3], $registro[4],$registro[5],$registro[6],$registro[7]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logadminDAO  -> registrar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logadminDAO -> consultarFiltro($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new LogAdmin($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
}

?>