<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogClienteDAO.php";



class LogCliente{
    
    private $id;
    private $id_cliente;
    private $fecha;
    private $hora;
    private $accion;
    private $nombre;
    private $apellido;
    private $correo;
    private $conexion;
    private $logclienteDAO;

    public function getId(){
        return $this -> id;
    }
    
    public function getId_cliente(){
        return $this -> id_cliente;
    }

    public function getFecha(){
        return $this -> fecha;
    }

    public function getHora(){
        return $this -> hora;
    }
    

    public function getAccion(){
        return $this -> accion;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    public function getApellido(){
        return $this -> apellido;
    }
    public function getCorreo(){
        return $this -> correo;
    }
    
    public function LogCliente($id = "", $fecha = "", $hora = "", $accion = "", $id_cliente="",$nombre="",$apellido="",$correo=""){
        $this -> id = $id;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> accion = $accion;
        $this -> id_cliente = $id_cliente;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;      
        $this -> conexion = new Conexion();
        $this -> logclienteDAO = new LogClienteDAO($this -> id, $this -> fecha, $this -> hora, $this ->accion,$this -> id_cliente,$this->nombre,$this->apellido,$this->correo);
    }


    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logclienteDAO -> consultar());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new LogCliente($registro[0], $registro[1], $registro[2], $registro[3], $registro[4],$registro[5],$registro[6],$registro[7]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    function consultarCliente(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logclienteDAO -> consultar());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new LogCliente($registro[0], $registro[1], $registro[2], $registro[3],"",$registro[5],$registro[6],$registro[7]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
      
    }
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logclienteDAO  -> registrar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logclienteDAO -> consultarFiltro($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new LogCliente($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
}

?>
