<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogChefDAO.php";



class LogChef{
    
    private $id;
    private $id_chef;
    private $fecha;
    private $hora;
    private $accion;
    private $nombre;
    private $apellido;
    private $correo;
    private $conexion;
    private $logchefDAO;

    public function getId(){
        return $this -> id;
    }
    
    public function getId_chef(){
        return $this -> id_chef;
    }

    public function getFecha(){
        return $this -> fecha;
    }

    public function getHora(){
        return $this -> hora;
    }
    

    public function getAccion(){
        return $this -> accion;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    public function getApellido(){
        return $this -> apellido;
    }
    public function getCorreo(){
        return $this -> correo;
    }
    
    public function LogChef($id = "", $fecha = "", $hora = "", $accion = "", $id_chef="",$nombre="",$apellido="",$correo=""){
        $this -> id = $id;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this -> accion = $accion;
        $this -> id_chef = $id_chef;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> correo = $correo;
        $this -> conexion = new Conexion();
        $this -> logchefDAO = new LogChefDAO($this -> id, $this -> fecha, $this -> hora, $this ->accion,$this -> id_chef);
    }


    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logchefDAO -> consultar());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new LogChef($registro[0], $registro[1], $registro[2], $registro[3], $registro[4],$registro[5],$registro[6],$registro[7]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    
    function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logchefDAO  -> registrar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> logchefDAO -> consultarFiltro($filtro));
        $logs = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new LogChef($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4], $resultado[5], $resultado[6], $resultado[7]);
            array_push($logs, $p);
        }
        $this -> conexion -> cerrar();
        return $logs;
    }
    
}

?>