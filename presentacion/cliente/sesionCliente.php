<?php
$cliente = new Cliente($_SESSION['id']);
$cliente->consultar();

date_default_timezone_set('UTC');
date_default_timezone_set("America/Bogota");
$fecha = date("Y-m-d");
$hora = date("H:i a");
$log = new LogCliente("", $fecha, $hora, "Inicia Sesion",$cliente->getId(), $cliente->getNombre(), $cliente->getApellido(), $cliente->getCorreo());
$log->registrar();


?>
<div class="container mt-4">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-dark text-white">Bienvenido Cliente</div>
				<div class="card-body">
					<p>Usuario: <?php echo $cliente -> getNombre() . " " . $cliente -> getApellido() ?></p>
					<p>Correo: <?php echo $cliente -> getCorreo(); ?></p>
					<p>Hoy es: <?php echo date("d-M-Y"); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>


