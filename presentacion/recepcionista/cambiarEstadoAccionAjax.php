<?php
$idReserva = $_GET['idReserva'];
$nuevoEstado = $_GET['nuevoEstado'];

echo  "<a id='cambiarEstado" . $idReserva . "' class='fas fa-power-off' href='#' data-toggle='tooltip' data-placement='left' title='" . ($nuevoEstado == 0 ? "Habilitar" : "Inhabilitar") . "'> </a>";
?>

<script>
$(document).ready(function(){
	$("#cambiarEstado<?php echo $idReserva ?>").click(function(e){
		$('[data-toggle="tooltip"]').tooltip('hide');
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/recepcionista/cambiarEstadoReservaAjax.php") ?>&idReserva=<?php echo $idReserva ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";		
		$("#icono<?php echo $idReserva ?>").load(url);
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/recepcionista/cambiarEstadoAccionAjax.php") ?>&idReserva=<?php echo $idReserva ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#accion<?php echo $idReserva ?>").load(url);
	});		
});
</script>
