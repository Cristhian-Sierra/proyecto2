<?php
$recepcionista = new Recepcionista($_SESSION['id']);
$recepcionista->consultar();

date_default_timezone_set('UTC');
date_default_timezone_set("America/Bogota");
$fecha = date("Y-m-d");
$hora = date("H:i a");
$log = new LogRecepcionista("", $fecha, $hora, "Inicia Sesion",$recepcionista->getId(), $recepcionista->getNombre(), $recepcionista->getApellido(), $recepcionista->getCorreo());
$log->registrar();

?>
<div class="container mt-4">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-dark text-white">Bienvenido Recepcionista</div>

				<div class="card-body">
					<p>Usuario: <?php echo $recepcionista -> getNombre() . " " . $recepcionista -> getApellido() ?></p>
					<p>Correo: <?php echo $recepcionista -> getCorreo(); ?></p>
					<p>Hoy es: <?php echo date("d-M-Y"); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>
