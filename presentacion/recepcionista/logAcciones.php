<?php
$log= new LogRecepcionista();
$logAccion= $log->consultar();

?>

<div class="container mt-4">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
		<input id="Filtro" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
		</div>
	</div>
</div>

<div class="container mt-4">
	<div class="row">
		<div class="col-12">
		<div id="resultadosLogs">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Log Acciones</h4>
				</div>
				<div class="text-right"><?php echo count($logAccion) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Fecha</th>
							<th>Hora</th>
							<th>Accion</th>
							<th>Id_Recepcionista</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correoo</th>
						</tr>
						<?php 
						$i=1;
						foreach($logAccion as $l){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $l -> getFecha() . "</td>";
						    echo "<td>" . $l -> getHora() . "</td>";
						    echo "<td>" . $l -> getAccion() . "</td>";
						    echo "<td>" . $l -> getId_recepcionista() . "</td>";
						    echo "<td>" . $l -> getNombre() . "</td>";
						    echo "<td>" . $l -> getApellido() . "</td>";
						    echo "<td>" . $l -> getCorreo() . "</td>";
						    ?>  
                     
						<?php   						    
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
		</div>
	</div>
</div>




        

<script type="text/javascript">
$(document).ready(function(){
	$("#Filtro").keyup(function(){
	     var fil = $("#Filtro").val();
	     console.log(fil);
	     if(fil.length>=1){
		     <?php echo "var ruta = \"indexAjax.php?pid=". base64_encode("presentacion/recepcionista/logAccionesAjax.php")."\";\n";?>
			 $("#resultadosLogs").load(ruta,{fil});
	     }
	
	});
});
</script>