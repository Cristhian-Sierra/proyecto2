<?php
$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}

$precio = "";
if(isset($_POST["precio"])){
    $precio = $_POST["precio"];
}    

$foto = "";
if(isset($_POST["foto"])){
    $foto = $_POST["foto"];
}


if(isset($_POST["crear"])){
	if($_FILES["foto"]["name"] != ""){
		$rutaLocal = $_FILES["foto"]["tmp_name"];
		$tipo = $_FILES["foto"]["type"];
		$tiempo = new DateTime();
		$rutaRemota = "fotos/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
		copy($rutaLocal,$rutaRemota);
    $plato = new Plato("", $nombre, $precio,$foto);
    if($plato-> getFoto() != ""){
    	unlink($plato -> getFoto());
    }
    $plato = new Plato("", $nombre,$precio,$rutaRemota);
    $plato -> registrar();
    }else{
    	$plato = new Plato("", $nombre,$precio);
    	$plato -> registrar();
    }
    date_default_timezone_set('UTC');
    date_default_timezone_set("America/Bogota");
    $fecha = date("Y-m-d");
    $hora = date("H:i a");
    $log = new LogAdmin("", $fecha, $hora, "Agregar plato",$administrador->getId(), $administrador->getNombre(), $administrador->getApellido(), $administrador->getCorreo());
    $log->registrar();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Agregar plato</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Plato, ingresado correctamente
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/administrador/agregarPlato.php") ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
					
						<div class="form-group">
							<label>Precio</label> 
							<input type="number" name="precio" class="form-control" min="1" value="<?php echo $precio ?>" required>
						</div>
						<div class="form-group">
							<label>Foto</label> 
							<input type="file" name="foto" class="form-control" >
						</div>
						<button type="submit" name="crear" class="btn btn-dark">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>