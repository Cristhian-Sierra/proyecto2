<?php
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();


date_default_timezone_set('UTC');
date_default_timezone_set("America/Bogota");
$fecha = date("Y-m-d");
$hora = date("H:i a");
$log = new LogAdmin("", $fecha, $hora, "Inicia Sesion",$administrador->getId(), $administrador->getNombre(), $administrador->getApellido(), $administrador->getCorreo());
$log->registrar();



?>
<div class="container mt-4">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-dark text-white">Bienvenido Administrador</div>
				<div class="card-body">
					<p>Nombre completo: <?php echo $administrador -> getNombre(). $administrador->getApellido()?></p>
					<p>Correo: <?php echo $administrador -> getCorreo(); ?></p>
					<p>Fecha: <?php echo date("d-M-Y"); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>