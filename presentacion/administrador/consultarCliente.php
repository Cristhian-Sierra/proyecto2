<?php
$cliente= new Cliente();
$clientes = $cliente -> consultarTodos();

$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();
date_default_timezone_set('UTC');
date_default_timezone_set("America/Bogota");
$fecha = date("Y-m-d");
$hora = date("H:i a");
$log = new LogAdmin("", $fecha, $hora, "Consulta Cliente",$administrador->getId(), $administrador->getNombre(), $administrador->getApellido(), $administrador->getCorreo());
$log->registrar();
?>

<div class="container mt-4">
	<div class="row">
		<div class="col-3"></div>
		<div class="col-6">
		<input id="Filtro" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
		</div>
	</div>
</div>

<div class="container mt-3">
	<div class="row">
		<div class="col">
			<div id="resultadosCliente">
				
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Consultar Cliente</h4>
				</div>
				<div class="text-right"><?php echo count($clientes) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Correo</th>
						
							<th>Estado</th>
							<th>Servicios</th>
						</tr>
						<?php 
						$i=1;
						foreach($clientes as $c){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $c -> getNombre() . "</td>";
						    echo "<td>" . $c -> getApellido() . "</td>";
						    echo "<td>" . $c -> getCorreo() . "</td>";
						    
						    echo "<td>" . (($c -> getEstado()==1)?"<div id='icono" . $c -> getId() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($c -> getEstado()==0)?"<div id='icono" . $c -> getId() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";						    
						    echo "<td> <div id='accion" . $c -> getId() . "'>" . "<a id='cambiarEstado" . $c->getId() . "' class='fas fa-power-off' href='#' data-toggle='tooltip' data-placement='left' title='" . ($c->getEstado() == 0 ? "Habilitar" : "Inhabilitar") . "'> </a>";
						    ;
						   
						    ?>  
                        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $c -> getId() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoClienteAjax.php") ?>&idCliente=<?php echo $c -> getId() ?>&nuevoEstado=<?php echo (($cliente -> getEstado()==1)?"0":"1")?>";		
                        		$("#icono<?php echo $c -> getId() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoAccionAjax.php") ?>&idCliente=<?php echo $c-> getId() ?>&nuevoEstado=<?php echo (($cliente -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php  echo $c -> getId() ?>").load(url);
                        	});
                        });
                        </script>
						<?php   						    
						    echo "</div></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
			</div>
		</div>
	</div>
</div>




        

<script type="text/javascript">
$(document).ready(function(){
	$("#Filtro").keyup(function(){
	     var fil = $("#Filtro").val();
	     console.log(fil);
	     if(fil.length>=1){
		     <?php echo "var ruta = \"indexAjax.php?pid=". base64_encode("presentacion/administrador/consultarClienteAjax.php")."\";\n";?>
			 $("#resultadosCliente").load(ruta,{fil});
	     }
	
	});
});
</script>