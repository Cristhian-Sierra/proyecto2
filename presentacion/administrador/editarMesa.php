<?php


$error = -1;
if (isset($_POST["editar"])) {
   
    $nombre = $_POST["nombre"];
    switch($_POST["numero"])
    {
        case 1:
            $numero = 2;
            break;
        case 2:
            $numero = 4;
            break;
        case 3:
            $numero = 8;
            break;
        case 4:
            $numero = 12;
            break;
        default:
            //No se selecciono ningun valor
            break;
    }
    
    $mesa = new Mesa($_GET['idmesa'], $_POST['nombre'], $_POST['numero']);
    $mesa->editar();
    $error=1;
    
   
    date_default_timezone_set('UTC');
    date_default_timezone_set("America/Bogota");
    $fecha = date("Y-m-d");
    $hora = date("H:i a");
    $log = new LogAdmin("", $fecha, $hora, "Editar mesa",$administrador->getId(), $administrador->getNombre(), $administrador->getApellido(), $administrador->getCorreo());
    $log->registrar();
}
else{
    $mesa = new Mesa($_GET['idmesa']);
    $mesa->consultar();
    
}
 ?>
    <div class="container my-4">
    <div class="row">
    <div class="col-3"></div>
    <div class="col-6">
    <div class="card">
    <div class="card-header bg-dark text-white">Edicion Mesa</div>
    <div class="card-body">
  		<?php if($error==1){ ?>
  		
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
						<form action="index.php?pid=<?php echo base64_encode("presentacion/administrador/editarMesa.php") ?>&idmesa=<?php echo $_GET['idmesa']?>" method="post" enctype="multipart/form-data">
						
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $mesa -> getNombre() ?>" required>
						</div>
						<div>
						<label>Ingrese Numero de Personas</label>
						<select name="numero">
                        <option value="1">2</option>
                        <option value="2">4</option>
                         <option value="3">8</option>
                        <option value="4">12</option>
                        </select>
						</div>
						<button type="submit" name="registrar" class="btn btn-dark">Editar</button>
					</form>
				</div>
			</div>
		</div>

	</div>

</div>

    
    
    
   