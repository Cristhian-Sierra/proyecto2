<?php
$cliente = new Cliente();
$clientes = $cliente->buscarCliente($_REQUEST["fil"]);
?>
<div class="card">
	<div class="card-header bg-dark text-white">Consultar Clientes</div>
	<div class="card-body">
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Nombre</th>
					<th scope="col">Apellido</th>
					<th scope="col">Correo</th>
					<th scope="col">Estado</th>
					
					<th scope="col">Servicios</th>
				</tr>
			</thead>
			<tbody>
						<?php
    foreach ($clientes as $c) {
        echo "<tr>";
        echo "<td>" . $c->getId() . "</td>";
        echo "<td>" . $c->getNombre() . "</td>";
        echo "<td>" . $c->getApellido() . "</td>";
        echo "<td>" . $c->getCorreo() . "</td>";
        echo "<td>" . (($c -> getEstado()==1)?"<div id='icono" . $c -> getId() . "'><span class='fas fa-check-circle' data-toggle='tooltip' data-placement='left' title='Habilitado'></span></div>":(($c -> getEstado()==0)?"<div id='icono" . $c -> getId() . "'><span class='fas fa-times-circle' data-toggle='tooltip' data-placement='left' title='Deshabilitado'></span></div>":"<span class='fas fa-ban' data-toggle='tooltip' data-placement='left' title='Inactivo'></span>")) . "</td>";
        
        echo "<td> <div id='accion" . $c -> getId() . "'>" . "<a id='cambiarEstado" . $c->getId() . "' class='fas fa-power-off' href='#' data-toggle='tooltip' data-placement='left' title='" . ($c->getEstado() == 0 ? "Habilitar" : "Inhabilitar") . "'> </a>";
        ?>
        <script>
                        $(document).ready(function(){
                        	$("#cambiarEstado<?php echo $c -> getId() ?>").click(function(e){
                        		$('[data-toggle="tooltip"]').tooltip('hide');
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoClienteAjax.php") ?>&idCliente=<?php echo $c -> getId() ?>&nuevoEstado=<?php echo (($cliente -> getEstado()==1)?"0":"1")?>";		
                        		$("#icono<?php echo $c -> getId() ?>").load(url);
                        		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoAccionAjax.php") ?>&idCliente=<?php echo $c-> getId() ?>&nuevoEstado=<?php echo (($cliente -> getEstado()==1)?"0":"1")?>";
                        		$("#accion<?php  echo $c -> getId() ?>").load(url);
                        	});
                        });
                        </script>
        <?php 
        echo "</div> </td></tr>";
    }

    echo "<tr><td colspan='9'>" . count($clientes) . " registros encontrados</td></tr>"?>	
						</tbody>
		</table>
	</div>
</div>


