<?php
$idCliente = $_GET['idCliente'];
$nuevoEstado = $_GET['nuevoEstado'];

echo  "<a id='cambiarEstado" . $idCliente . "' class='fas fa-power-off' href='#' data-toggle='tooltip' data-placement='left' title='" . ($nuevoEstado == 0 ? "Habilitar" : "Inhabilitar") . "'> </a>";
;


?>

<script>
$(document).ready(function(){
	$("#cambiarEstado<?php echo $idCliente ?>").click(function(e){
		$('[data-toggle="tooltip"]').tooltip('hide');
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoClienteAjax.php") ?>&idCliente=<?php echo $idCliente ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";		
		$("#icono<?php echo $idCliente ?>").load(url);
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/administrador/cambiarEstadoAccionAjax.php") ?>&idCliente=<?php echo $idCliente ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#accion<?php echo $idCliente ?>").load(url);
	});		
});
</script>
