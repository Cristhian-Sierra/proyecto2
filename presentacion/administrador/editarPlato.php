<?php
if(isset($_POST["editar"])){
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "fotos/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        $plato = new Plato($_GET['idplato']);
        $plato -> consultar();
        if($plato -> getFoto() != ""){
            unlink($plato-> getFoto());
        }
        $plato = new Plato($_GET['idplato'], $_POST['nombre'], $_POST['precio'], $rutaRemota);
        $plato-> editar();
    }else{
        $plato = new Plato($_GET['idplato'], $_POST['nombre'], $_POST['precio']);
        $plato -> editar();      
    }
    date_default_timezone_set('UTC');
    date_default_timezone_set("America/Bogota");
    $fecha = date("Y-m-d");
    $hora = date("H:i a");
    $log = new LogAdmin("", $fecha, $hora, "Editar plato",$administrador->getId(), $administrador->getNombre(), $administrador->getApellido(), $administrador->getCorreo());
    $log->registrar();
    
}else{
    $plato = new Plato($_GET['idplato']);
    $plato -> consultar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Editar Plato</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["editar"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos editados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/administrador/editarPlato.php") ?>&idplato=<?php echo $_GET['idplato']?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $plato -> getNombre() ?>" required>
						</div>
						
						<div class="form-group">
							<label>Precio</label> 
							<input type="number" name="precio" class="form-control" min="1" value="<?php echo $plato -> getPrecio() ?>" required>
						</div>
						<div class="form-group">
							<label>Foto</label> 
							<input type="file" name="imagen" class="form-control" >
						</div>
						<button type="submit" name="editar" class="btn btn-dark">Editar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>