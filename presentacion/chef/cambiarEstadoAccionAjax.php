<?php
$idPedido = $_GET['idPedido'];
$nuevoEstado = $_GET['nuevoEstado'];

echo  "<a id='cambiarEstado" . $idPedido . "' class='fas fa-power-off' href='#' data-toggle='tooltip' data-placement='left' title='" . ($nuevoEstado == 0 ? "Habilitar" : "Inhabilitar") . "'> </a>".
    " <a href='indexAjax.php?pid=". base64_encode("presentacion/modalPedido.php") . "&idPedido=" . $idPedido . "' data-toggle='modal' data-target='#modalPedido' ><span class='fas fa-eye' data-toggle='tooltip' class='tooltipLink' data-placement='left' data-original-title='Ver Detalles' ></span> </a>"; 
?>

<script>
$(document).ready(function(){
	$("#cambiarEstado<?php echo $idPedido ?>").click(function(e){
		$('[data-toggle="tooltip"]').tooltip('hide');
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/chef/editarEstadoPedidoAjax.php") ?>&idPedido=<?php echo $idPedido ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";		
		$("#icono<?php echo $idPedido ?>").load(url);
		var url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/chef/cambiarEstadoAccionAjax.php") ?>&idPedido=<?php echo $idPedido ?>&nuevoEstado=<?php echo (($nuevoEstado==1)?"0":"1")?>";
		$("#accion<?php echo $idPedido ?>").load(url);
	});		
});
</script>
