<?php
$chef = new Chef($_SESSION['id']);
$chef->consultar();


date_default_timezone_set('UTC');
date_default_timezone_set("America/Bogota");
$fecha = date("Y-m-d");
$hora = date("H:i a");
$log = new LogChef("", $fecha, $hora, "Inicia Sesion",$chef->getId(), $chef->getNombre(), $chef->getApellido(), $chef->getCorreo());
$log->registrar();

?>
<div class="container mt-4">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-dark text-white">Bienvenido Chef</div>
				<div class="card-body">
					<p>Usuario: <?php echo $chef -> getNombre() . " " . $chef -> getApellido() ?></p>
					<p>Correo: <?php echo $chef -> getCorreo(); ?></p>
					<p>Hoy es: <?php echo date("d-M-Y"); ?></p>
				</div>
			</div>
		</div>
	</div>
</div>



