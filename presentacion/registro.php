<?php 
include "presentacion/encabezado.php";
$error = 0;
$registrado = false;

if(isset($_POST["registrar"])){
	$nombre = $_POST["nombre"];
	$apellido = $_POST["apellido"];
    $correo = $_POST["correo"];
    $clave = $_POST["clave"];
   // $foto = $_POST["foto"];
    
    $cliente = new Cliente("", "$nombre", "$apellido", "$correo", "$clave");
    if($cliente -> existeCorreo()){
        $error = 1;
    }
    else{
    	$cliente = new Cliente("", $nombre, $apellido, $correo,$clave);
    	$cliente -> registrar();
    }
       
        $registrado = true;
    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-dark">
					<h4>Registro</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/registro.php") ?>" method="post" enctype="multipart/form-data">
        				<div class="form-group">
    						<input name="nombre" type="text" class="form-control" placeholder="Nombres" required>
    					</div>
        				<div class="form-group">
    						<input name="apellido" type="text" class="form-control" placeholder="Apellidos" required>
    					</div>
        				<div class="form-group">
    						<input name="correo" type="email" class="form-control" placeholder="Correo" required>
    					</div>
        				<div class="form-group">
    						<input name="clave" type="password" class="form-control" placeholder="Clave" required>
    					</div>
        					
        				<div class="form-group">
    						<input name="registrar" type="submit" class="form-control btn btn-dark">
    						
    					</div>
    						<a class="btn btn-dark" href="index.php" role="button">Inicio</a>    					
        			</form>
        		   
        			<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						El correo <?php echo $correo ?> ya se encuentra registrado.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($registrado) { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						El  usuario <?php echo $nombre,$apellido ?> ha sido registrado correctamente.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
            	</div>
            </div>		
		</div>
	</div>
</div>


