<?php
session_start();
require 'logica/Persona.php';
require 'logica/Administrador.php';
require 'logica/Recepcionista.php';
require 'logica/Cliente.php';
require 'logica/Chef.php';
require 'logica/Factura.php';
require 'logica/Mesa.php';
require 'logica/Pedido.php';
require 'logica/Plato.php';
require 'logica/Reserva.php';
require 'logica/Pedido_Plato.php';
require 'logica/LogAdmin.php';
require 'logica/LogCliente.php';
require 'logica/LogChef.php';
require 'logica/LogRecepcionista.php';
//require_once 'logica/Proceso.php';
//require_once 'logica/Actividad.php';

$pid = "";
if(isset($_GET["pid"])){
    $pid = base64_decode($_GET["pid"]);
}else{
    $_SESSION['id']="";
    $_SESSION["rol"]="";
}
if(isset($_GET["cerrarSesion"]) || !isset($_SESSION['id'])){
    $_SESSION['id']="";
}
?>
<head>
<title>SGRRV (Reservas Restaurante Vegetariano)</title>
<link rel="icon"  href="imagen/icono.ico">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/chartkick/2.3.0/chartkick.min.js"></script>
<script src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
})
</script>

</head>


<body>
	<?php 
	$paginasSinSesion = array(
        "presentacion/autenticar.php",
	    "presentacion/registro.php",
	  
	);	
	if(in_array($pid, $paginasSinSesion)){
	    include $pid;
	}
	else if($_SESSION['id']!="") {
	    if($_SESSION["rol"] == "Administrador"){
	        include "presentacion/administrador/menuAdministrador.php";
	    }
	    else if($_SESSION["rol"]=="Chef"){
	    	include 'presentacion/chef/menuChef.php';
	    	
	    }
	    
	    else if($_SESSION["rol"] == "Cliente"){
	        include "presentacion/cliente/menuCliente.php";
	    }
	    else if($_SESSION["rol"] == "Recepcionista"){
	        include "presentacion/recepcionista/menuRecepcionista.php";
	    }
	    include $pid;
	}else{
	    include "presentacion/encabezado.php";
	    include "presentacion/inicio.php";
	}
	?>	
</body>