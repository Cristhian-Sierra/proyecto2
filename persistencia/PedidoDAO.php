<?php

class PedidoDAO {
    private $idpedido;
    private $reserva_idreserva;
    private $chef_idchef;
    private $estado;
    
    function PedidoDAO ($idpedido, $reserva_idreserva, $chef_idchef, $estado){
        $this -> idpedido = $idpedido;
        $this -> reserva_idreserva = $reserva_idreserva;
        $this -> chef_idchef = $chef_idchef;
        $this -> estado = $estado;
        
    }
    
    function consultar(){
        return "select r.id, p.estado
                from pedido as p,reserva as r
                where r.id=p.id_reserva and p.id =" . $this -> idpedido;
    }
    function consultarTodos(){
        return "select id, id_reserva,id_chef, estado
                from pedido";
    }
    function consultarPedidoCliente($idCliente) {
        return "select p.id, p.id_reserva, p.id_chef, p.estado
                from pedido as p, reserva as r, cliente as c where p.id_reserva=r.id AND r.id_cliente=c.id AND c.id=".$idCliente;
    }
    
    function consultarId(){
        return "select id
                from pedido
                where id_reserva = " . $this -> reserva_idreserva ;
    }
    
    function actualizarEstado(){
        return "update pedido set
                estado = " . $this -> estado . "
                where id=" . $this -> idpedido;
    }
    
    function actualizarChef(){
        return "update pedido set
                id_chef = ".$this->chef_idchef."
                where id=" . $this -> idpedido;
    }
    
    function buscarPedido($filtro){
        return "select  id, id_reserva, id_chef, estado
                from pedido
                where id_reserva like '%" . $filtro . "%'";
        
    }
    function buscarPedidoCliente($filtro,$idCliente){
        return "select p.id, p.id_reserva, p.id_chef, p.estado
                from pedido as p, reserva as r, cliente as c
                where p.id_reserva=r.id AND r.id_cliente=c.id AND c.id=".$idCliente." AND id_reserva like '%" . $filtro . "%'";
        
    }
    
    function registrar(){
        return "INSERT INTO pedido(id_reserva,estado) VALUES(" . $this->reserva_idreserva . ",". 0 .")";
    }
    
    function consultarPedidoChef(){
        return "select id, id_reserva, estado
                from pedido
                where id_chef=". $this -> chef_idchef;
    }
   
    
    
}

?>


