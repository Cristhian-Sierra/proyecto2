<?php
class Pedido_PlatoDAO {
    private $pedido_idpedido;
    private $plato_idplato;
    private $cantidad;
    private $descripcion;
    
    function Pedido_PlatoDAO ($pedido_idpedido, $plato_idplato, $cantidad, $descripcion){
        $this -> pedido_idpedido = $pedido_idpedido;
        $this -> plato_idplato = $plato_idplato;
        $this -> cantidad = $cantidad;
        $this -> descripcion = $descripcion;
    }
    
    function consultar(){
        return "select pp.id_pedido, pp.id_plato, pp.cantidad, pp.descripcion
                from pedido_plato as pp,pedido as pe,plato as p
                where pp.id_pedido=pe.id and pp.id_plato=p.id 
                and pp.id_pedido = '" . $this -> pedido_idpedido . "' and pp.id_plato = '". $this -> plato_idplato ."'";
    }
    
    function registrar(){
        return "INSERT INTO pedido_plato (id_pedido, id_plato, cantidad, descripcion)
                VALUES ('" . $this -> pedido_idpedido . "', '" . $this -> plato_idplato . "',' " . $this -> cantidad . "', '" . $this -> descripcion . "');";
    }
    function consultarReservaPlato() {
        return "select p.id, pp.cantidad, pp.descripcion
                from plato as p,pedido_plato as pp
                where p.id=pp.id_plato and pp.id_pedido = " . $this -> pedido_idpedido;
    }
    
    function consultarModalReservaPlato() {
        return "select p.nombre, pp.cantidad, pp.descripcion
                from plato as p,pedido_plato as pp
                where p.id=pp.id_plato and pp.id_pedido = " . $this -> pedido_idpedido;
    }
    
    function consultarPlatoVendido(){
        return "select n.nombre,  Sum(p.cantidad) AS total
                from pedido_plato p, plato n
                where p.id_plato = p.id_plato and p.id_plato = n.id
                group by p.id_plato;";
    }
    
    function consultarFactura(){
        
        return "select pe.id,p.nombre,p.precio,pp.cantidad,pp.descripcion 
                  from pedido_plato as pp,plato as p, pedido as pe  where pe.id=pp.id_pedido and pp.id_plato=p.id 
                   and pp.id_pedido= '".$this->pedido_idpedido."'";
            
        
    }
    
}
