<?php

class MesaDAO {
    private $idmesa;
    private $nombre;
    private $numero_personas;
    
    function MesaDAO ($idmesa, $nombre, $numero_personas){
        $this -> idmesa = $idmesa;
        $this -> nombre = $nombre;
        $this -> numero_personas = $numero_personas;
        
    }
    
    function consultar(){
        return "select id, nombre, numero_personas
                from mesa
                where id = '" . $this -> idmesa . "'";
    }
    
    function consultarTodos(){
        return "select id, nombre, numero_personas
                from mesa";
    }
    
    function buscarMesa($filtro){
        return "select id,nombre, numero_personas
                from mesa
                where  id like '%" . $filtro . "%' or nombre like '%" . $filtro . "%'";
        
    }
    
    function registrar(){
        return "insert into mesa (nombre,numero_personas)
                values ('". $this -> nombre ."', ". $this -> numero_personas.")";
        
    }
    
    public function editar(){
        return "update mesa
                set nombre = '" . $this -> nombre . "', numero_personas = '" . $this -> numero_personas . "' 
                where id = '" . $this -> idmesa .  "'";
    }
    
}