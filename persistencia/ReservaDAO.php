<?php

class ReservaDAO {
    private $idreserva;
    private $hora;
    private $fecha;
    private $cliente_idcliente;
    private $mesa_idmesa;
    private $recepcionista_idrecepcionista;
    private $estado;
    
    function ReservaDAO($idreserva, $hora, $fecha, $cliente_idcliente, $mesa_idmesa, $recepcionista_idrecepcionista, $estado){
        $this -> idreserva = $idreserva;
        $this -> hora = $hora;
        $this -> fecha = $fecha;
        $this -> cliente_idcliente = $cliente_idcliente;
        $this -> recepcionista_idrecepcionista = $recepcionista_idrecepcionista;
        $this -> mesa_idmesa = $mesa_idmesa;
        $this -> estado = $estado;
    }
    
    function consultar() {
        return "select id, hora, fecha, id_cliente, id_mesa, id_recepcionista, estado
                from reserva
                where id =" . $this -> idreserva;
    }
    
    function consultarReservaCliente() {
            return "select re.id, re.hora, re.fecha, c.nombre, m.id, r.correo, re.estado
                from reserva as re,recepcionista as r,mesa as m,cliente as c
                where m.id=re.id_mesa and re.id_recepcionista=r.id and c.id=re.id_cliente and id_cliente =" . $this -> cliente_idcliente;
    }
    
    function registrar(){
        return "INSERT INTO reserva (hora,fecha,id_cliente, id_mesa, id_recepcionista,estado)
                VALUES ('".$this -> hora."','".$this -> fecha."',".$this ->cliente_idcliente.",".$this ->mesa_idmesa.",".$this ->recepcionista_idrecepcionista.",0) ";
    }

    
    function consultarTodos() {
        return "select reserva.id, reserva.hora, reserva.fecha, cliente.correo, mesa.id, recepcionista.correo,reserva.estado
                from reserva,cliente,mesa,recepcionista
                where cliente.id = reserva.id_cliente and mesa.id=reserva.id_mesa and recepcionista.id= reserva.id_recepcionista
                order by fecha DESC;";
    }
    
    function consultarRecepcionista() {
        return "select reserva.id, reserva.hora, reserva.fecha, cliente.nombre, mesa.id , recepcionista.correo, reserva.estado
                from reserva,cliente,mesa,recepcionista
                where recepcionista.id=reserva.id_recepcionista and cliente.id = reserva.id_cliente and mesa.id=reserva.id_mesa and reserva.id_recepcionista=". $this->recepcionista_idrecepcionista. "
                order by fecha DESC";
    }
    
    function buscarReserva($filtro){
        return "select reserva.id, reserva.hora, reserva.fecha, cliente.nombre, mesa.id, recepcionista.correo,reserva.estado
                from reserva , cliente, recepcionista, mesa
                where  (fecha like '%" . $filtro . "%' or
                hora like '%" . $filtro . "%' or
                id_cliente like '%". $filtro ."%' or
                cliente.nombre like '%". $filtro ."%'or
                recepcionista.correo like '%". $filtro ."%')and reserva.id_cliente=cliente.id and reserva.id_mesa=mesa.id and reserva.id_recepcionista=recepcionista.id
                order by fecha DESC";
        
    }
    function buscarReservaCliente($filtro,$idCliente){
        return "select reserva.id, reserva.hora, reserva.fecha, cliente.nombre, mesa.id, recepcionista.correo,reserva.estado
                from reserva , cliente, recepcionista, mesa
                where  (fecha like '%" . $filtro . "%' or
                hora like '%" . $filtro . "%' or
                id_cliente like '%". $filtro ."%' )and reserva.id_cliente=cliente.id and reserva.id_mesa=mesa.id and reserva.id_recepcionista=recepcionista.id and cliente.id=".$idCliente."
                order by fecha DESC";
        
    }
    function actualizarEstado(){
        return "update reserva set
                estado = '" . $this -> estado . "'
                where id=" . $this -> idreserva;
    }
    
    function ValidarReserva(){    
        return "SELECT id
                FROM reserva
                where hora= '". $this -> hora ."' and fecha= '". $this -> fecha ."' and id_mesa= ". $this -> mesa_idmesa ;
    }
    
    function consultarReserva(){
        return "select fecha,count(id)
                from reserva 
                where fecha = fecha
                group by fecha ";
    }
    
    function consultarDia(){
        return " SELECT DAYNAME(fecha), COUNT(id) FROM reserva WHERE DAYNAME(fecha) = DAYNAME(fecha) group by DAYNAME(fecha) ";
    }
    function consultarIdreserva() {
        return "SELECT max(id) FROM reserva";
    }
}


?>

